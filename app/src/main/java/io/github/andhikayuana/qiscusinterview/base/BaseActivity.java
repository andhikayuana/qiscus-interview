package io.github.andhikayuana.qiscusinterview.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(setView());

        initView();

        initPresenter();
    }

    protected abstract void initPresenter();

    protected abstract void initView();

    protected abstract int setView();

    protected void gotoActivity(Context context, Class<?> cls, boolean isClear) {
        Intent intent = new Intent(context, cls);
        if (isClear) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(intent);
    }

    protected void gotoActivity(Context context, Class<?> cls) {
        gotoActivity(context, cls, true);
    }
}
