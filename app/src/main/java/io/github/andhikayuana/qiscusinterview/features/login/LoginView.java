package io.github.andhikayuana.qiscusinterview.features.login;

import io.github.andhikayuana.qiscusinterview.base.BaseView;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public interface LoginView extends BaseView {

    void loginSuccess();
}
