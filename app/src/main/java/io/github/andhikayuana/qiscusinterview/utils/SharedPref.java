package io.github.andhikayuana.qiscusinterview.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.github.andhikayuana.qiscusinterview.App;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class SharedPref {

    private static SharedPreferences getPref() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }

    public static SharedPreferences.Editor getEditor() {
        return getPref().edit();
    }

    public static void saveBoolean(String key, boolean val) {
        getEditor().putBoolean(key, val).commit();
    }

    public static boolean getBoolean(String key) {
        return getPref().getBoolean(key, false);
    }

    public static void saveString(String key, String val) {
        getEditor().putString(key, val).commit();
    }

    public static String getString(String key) {
        return getPref().getString(key, null);
    }
}
