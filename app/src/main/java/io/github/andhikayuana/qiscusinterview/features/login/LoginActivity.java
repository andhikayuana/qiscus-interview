package io.github.andhikayuana.qiscusinterview.features.login;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import io.github.andhikayuana.qiscusinterview.R;
import io.github.andhikayuana.qiscusinterview.base.BaseActivity;
import io.github.andhikayuana.qiscusinterview.features.home.HomeActivity;
import io.github.andhikayuana.qiscusinterview.features.register.RegisterActivity;

public class LoginActivity extends BaseActivity implements LoginView, View.OnClickListener {

    private LoginPresenter mPresenter;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnLogin;
    private TextView tvRegister;

    @Override
    protected void initPresenter() {
        mPresenter = new LoginPresenter(this);
        mPresenter.initDb(this);
    }

    @Override
    protected void initView() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvRegister = (TextView) findViewById(R.id.tvRegister);

        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    protected int setView() {
        return R.layout.activity_login;
    }

    @Override
    public void message(String msg) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:

                mPresenter.login(
                        etEmail.getText().toString(),
                        etPassword.getText().toString()
                );

                break;

            case R.id.tvRegister:
                gotoActivity(this, RegisterActivity.class);
                break;
        }
    }

    @Override
    public void loginSuccess() {
        gotoActivity(this, HomeActivity.class);
    }
}
