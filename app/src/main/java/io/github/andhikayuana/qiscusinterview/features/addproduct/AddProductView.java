package io.github.andhikayuana.qiscusinterview.features.addproduct;

import io.github.andhikayuana.qiscusinterview.base.BaseView;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public interface AddProductView extends BaseView {
    void successSaveProduct();
}
