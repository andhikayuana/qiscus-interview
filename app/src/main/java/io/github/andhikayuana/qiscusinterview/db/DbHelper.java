package io.github.andhikayuana.qiscusinterview.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import io.github.andhikayuana.qiscusinterview.models.User;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, Db.NAME, null, Db.VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(Db.UserTable.CREATE);
            db.execSQL("INSERT INTO user VALUES ('jarjit@domain.com', '123');");

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }

    public boolean save(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = Db.UserTable.toContentValues(user);

        db.insert(Db.UserTable.TABLE_NAME, null, values);
        db.close();

        return true;
    }

    public User getByEmail(String email) {
        SQLiteDatabase db = this.getReadableDatabase();

        String sql = "SELECT * FROM " + Db.UserTable.TABLE_NAME + " WHERE " + Db.UserTable.FIELD_EMAIL +
                "= '" + email + "';";

        Cursor cursor = db.rawQuery(sql, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        User user = Db.UserTable.parseCursor(cursor);
        db.close();

        return user;
    }
}
