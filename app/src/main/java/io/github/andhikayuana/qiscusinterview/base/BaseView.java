package io.github.andhikayuana.qiscusinterview.base;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public interface BaseView {

    void message(String msg);
}
