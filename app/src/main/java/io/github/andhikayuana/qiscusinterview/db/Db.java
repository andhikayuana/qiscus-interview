package io.github.andhikayuana.qiscusinterview.db;

import android.content.ContentValues;
import android.database.Cursor;

import io.github.andhikayuana.qiscusinterview.models.User;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class Db {

    static final int VERSION = 1;

    static final String NAME = "db_docotel";

    public static abstract class UserTable {
        static final String TABLE_NAME = "user";

        static final String FIELD_EMAIL = "user_email";
        static final String FIELD_PASSWORD = "user_password";

        static final String CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                FIELD_EMAIL + " TEXT NOT NULL, " +
                FIELD_PASSWORD + " TEXT NOT NULL " + ");";

        public static ContentValues toContentValues(User user) {
            ContentValues values = new ContentValues();
            values.put(FIELD_EMAIL, user.getEmail());
            values.put(FIELD_PASSWORD, user.getPassword());
            return values;
        }

        public static User parseCursor(Cursor cursor) {
            return new User(
                    cursor.getString(cursor.getColumnIndexOrThrow(FIELD_EMAIL)),
                    cursor.getString(cursor.getColumnIndexOrThrow(FIELD_PASSWORD))
            );
        }
    }
}
