package io.github.andhikayuana.qiscusinterview.features.addproduct;

import android.text.TextUtils;

import java.util.HashMap;

import io.github.andhikayuana.qiscusinterview.networks.RestClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class AddProdcutPresenter {
    private final AddProductView mView;

    public AddProdcutPresenter(AddProductView view) {
        mView = view;
    }

    public void saveProduct(String productName, String productPrice) {

        if (TextUtils.isEmpty(productName)) {
            mView.message("Product Name tidak boleh kosong");
            return;
        }

        if (TextUtils.isEmpty(productPrice)) {
            mView.message("Product Price tidak boleh kosong");
            return;
        }


        RestClient
                .getApi()
                .postProduct(getInput(productName, productPrice))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {


                            if (response.isSuccessful()) {

                                mView.message("Berhasil menambahkan data");
                                mView.successSaveProduct();

                            } else {
                                // TODO: 4/5/17
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.printStackTrace();
                    }
                });


    }

    private HashMap<String, String> getInput(String productName, String productPrice) {
        HashMap<String, String> input = new HashMap<>();
        input.put("product[name]", productName);
        input.put("product[price]", productPrice);
        return input;
    }
}
