package io.github.andhikayuana.qiscusinterview.networks;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public interface Endpoints {

    @GET("products")
    Call<ResponseBody> getProducts();

    @FormUrlEncoded
    @POST("products")
    Call<ResponseBody> postProduct(@FieldMap HashMap<String, String> payload);

}
