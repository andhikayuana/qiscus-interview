package io.github.andhikayuana.qiscusinterview.features.addproduct;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.github.andhikayuana.qiscusinterview.R;
import io.github.andhikayuana.qiscusinterview.base.BaseActivity;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class AddProductActivity extends BaseActivity implements View.OnClickListener, AddProductView {

    private EditText etProductName;
    private EditText etProductPrice;
    private Button btnProductSave;
    private AddProdcutPresenter mPresenter;

    @Override
    protected void initPresenter() {
        mPresenter = new AddProdcutPresenter(this);
    }

    @Override
    protected void initView() {
        etProductName = (EditText) findViewById(R.id.etProductName);
        etProductPrice = (EditText) findViewById(R.id.etProductPrice);
        btnProductSave = (Button) findViewById(R.id.btnProductSave);
        btnProductSave.setOnClickListener(this);
    }

    @Override
    protected int setView() {
        return R.layout.activity_add_product;
    }

    @Override
    public void onClick(View view) {
        mPresenter.saveProduct(
                etProductName.getText().toString(),
                etProductPrice.getText().toString()
        );
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successSaveProduct() {
        finish();
    }
}
