package io.github.andhikayuana.qiscusinterview.features.home;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.github.andhikayuana.qiscusinterview.helper.Helper;
import io.github.andhikayuana.qiscusinterview.models.Product;
import io.github.andhikayuana.qiscusinterview.networks.Endpoints;
import io.github.andhikayuana.qiscusinterview.networks.RestClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class HomePresenter {

    private final HomeView mView;
    private Endpoints mApi;

    public HomePresenter(HomeView view) {
        mView = view;
    }

    public void getProducs() {
        RestClient
                .getApi()
                .getProducts()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {


                            if (response.isSuccessful()) {

                                String res = response.body().string();

                                JsonArray jRes = Helper.getGsonInstance().fromJson(res, JsonArray.class);

                                Type listType = new TypeToken<List<Product>>() {
                                }.getType();
                                List<Product> productList = Helper
                                        .getGsonInstance()
                                        .fromJson(jRes, listType);


                                mView.showData(productList);

                            } else {

                                String res = response.errorBody().string();

                                Log.e("ERROR", res);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });


    }
}
