package io.github.andhikayuana.qiscusinterview;

import android.app.Application;
import android.content.Context;

import io.github.andhikayuana.qiscusinterview.utils.SharedPref;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class App extends Application {

    private static Context context;

    public static synchronized Context getContext() {
        return context;
    }

    public static void logout() {
        SharedPref.getEditor().clear().commit();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
}
