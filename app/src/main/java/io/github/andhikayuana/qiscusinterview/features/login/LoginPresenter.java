package io.github.andhikayuana.qiscusinterview.features.login;

import android.content.Context;
import android.text.TextUtils;

import io.github.andhikayuana.qiscusinterview.db.DbHelper;
import io.github.andhikayuana.qiscusinterview.models.User;
import io.github.andhikayuana.qiscusinterview.utils.SharedPref;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class LoginPresenter {

    private final LoginView mView;
    private DbHelper mDb;

    public LoginPresenter(LoginView view) {
        mView = view;

        if (SharedPref.getBoolean("LOGIN")) {
            mView.loginSuccess();
        }
    }

    public void initDb(Context context) {
        mDb = new DbHelper(context);
    }

    public void login(String email, String password) {
        if (TextUtils.isEmpty(email)) {
            mView.message("Email tidak boleh kosong");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            mView.message("Password tidak boleh kosong");
            return;
        }

        if (userLogin(email, password)) {
            mView.loginSuccess();
        }
    }

    private boolean userLogin(String email, String password) {
        try {

            User user = mDb.getByEmail(email);

            if (user.getPassword() == password) {
                mView.message("Password Salah");
                return false;
            } else {
                SharedPref.saveBoolean("LOGIN", true);
                SharedPref.saveString("email", user.getEmail());

                return true;
            }

        } catch (Exception e) {
            mView.message("email salah");
            return false;
        }
    }
}
