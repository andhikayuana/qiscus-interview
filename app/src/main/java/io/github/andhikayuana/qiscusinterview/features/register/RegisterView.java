package io.github.andhikayuana.qiscusinterview.features.register;

import io.github.andhikayuana.qiscusinterview.base.BaseView;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public interface RegisterView extends BaseView {
    void registerSuccess();
}
