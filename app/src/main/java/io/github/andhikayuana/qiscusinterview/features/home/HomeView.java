package io.github.andhikayuana.qiscusinterview.features.home;

import java.util.List;

import io.github.andhikayuana.qiscusinterview.models.Product;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public interface HomeView {

    void showData(List<Product> productList);
}
