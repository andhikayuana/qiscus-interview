package io.github.andhikayuana.qiscusinterview.features.register;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.github.andhikayuana.qiscusinterview.R;
import io.github.andhikayuana.qiscusinterview.base.BaseActivity;
import io.github.andhikayuana.qiscusinterview.features.login.LoginActivity;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener, RegisterView {

    private EditText etEmail;
    private EditText etPassword;
    private Button btnRegister;
    private TextView tvLogin;
    private RegisterPresenter mPresenter;

    @Override
    protected void initPresenter() {
        mPresenter = new RegisterPresenter(this);
        mPresenter.initDb(this);
    }

    @Override
    protected void initView() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        tvLogin = (TextView) findViewById(R.id.tvLogin);

        btnRegister.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    protected int setView() {
        return R.layout.activity_register;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:

                mPresenter.register(
                        etEmail.getText().toString(),
                        etPassword.getText().toString());

                break;
            case R.id.tvLogin:

                gotoActivity(this, LoginActivity.class);

                break;

        }
    }

    @Override
    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void registerSuccess() {
        gotoActivity(this, LoginActivity.class);
    }
}
