package io.github.andhikayuana.qiscusinterview.features.home;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import io.github.andhikayuana.qiscusinterview.App;
import io.github.andhikayuana.qiscusinterview.R;
import io.github.andhikayuana.qiscusinterview.base.BaseActivity;
import io.github.andhikayuana.qiscusinterview.features.addproduct.AddProductActivity;
import io.github.andhikayuana.qiscusinterview.features.login.LoginActivity;
import io.github.andhikayuana.qiscusinterview.models.Product;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class HomeActivity extends BaseActivity implements HomeView, View.OnClickListener {

    private HomePresenter mPresenter;
    private Button btnLogout;
    private ListView lvProducts;
    private HomeAdapter mAdapter;
    private Button btnAddProduct;

    @Override
    protected void initPresenter() {
        mPresenter = new HomePresenter(this);
        mPresenter.getProducs();
    }

    @Override
    protected void initView() {
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnAddProduct = (Button) findViewById(R.id.btnAddProduct);
        lvProducts = (ListView) findViewById(R.id.lvProducts);

        btnLogout.setOnClickListener(this);
        btnAddProduct.setOnClickListener(this);
    }

    @Override
    protected int setView() {
        return R.layout.activity_home;
    }

    @Override
    public void showData(List<Product> productList) {

        mAdapter = new HomeAdapter(this, productList);
        lvProducts.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnAddProduct:

                gotoActivity(this, AddProductActivity.class, false);

                break;

            case R.id.btnLogout:

                App.logout();
                gotoActivity(this, LoginActivity.class);
                break;
        }

    }
}
