package io.github.andhikayuana.qiscusinterview.features.register;

import android.content.Context;
import android.text.TextUtils;

import io.github.andhikayuana.qiscusinterview.db.DbHelper;
import io.github.andhikayuana.qiscusinterview.models.User;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class RegisterPresenter {

    private final RegisterView mView;
    private DbHelper mDb;

    public RegisterPresenter(RegisterView view) {
        mView = view;
    }

    public void register(String email, String password) {

        if (TextUtils.isEmpty(email)) {
            mView.message("Email tidak boleh kosong");
            return;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView.message("Email tidak valid");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            mView.message("Password tidak boleh kosong");
            return;
        }

        if (userRegister(email, password)) {
            mView.registerSuccess();
        } else {
            mView.message("Gagal melakukan registrasi");
        }

    }

    private boolean userRegister(String email, String password) {
        return mDb.save(new User(email, password));
    }

    public void initDb(Context context) {
        mDb = new DbHelper(context);
    }
}
