package io.github.andhikayuana.qiscusinterview.features.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import io.github.andhikayuana.qiscusinterview.R;
import io.github.andhikayuana.qiscusinterview.models.Product;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 4/5/17
 */

public class HomeAdapter extends ArrayAdapter<Product> {

    public HomeAdapter(Context context, List<Product> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_product, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tvProductName.setText(getItem(position).getName());
        holder.tvProductPrice.setText("Rp " + getItem(position).getPrice());


        return view;
    }

    private class ViewHolder {

        private final TextView tvProductName;
        private final TextView tvProductPrice;

        public ViewHolder(View view) {
            tvProductName = (TextView) view.findViewById(R.id.tvProductName);
            tvProductPrice = (TextView) view.findViewById(R.id.tvProductPrice);
        }
    }
}
